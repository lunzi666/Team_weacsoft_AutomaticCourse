package com.spring.dao;

import com.spring.entity.PageBean;
import com.spring.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository("UserDao")
@Mapper
public interface UserDao {
    List<User> selectAllUser();
    User selectUserByIdentityId(String id);
    User selectUserByUsername(String username);
    User selectUserByOpenid(String openid);
    void insertUser(User user);
    void insertWebUser(User user);
    void insertWXUser(User user);
    void updateUserInfoByID(User user);
    void updateInfoByMyself(User user);
    User selectUserById(String id);
    List<User> selectUserByPage(HashMap<String,Object> map);
    int selectCountByUser();
}