package com.spring.service;

import com.spring.entity.PageBean;
import com.spring.entity.User;

import java.util.List;

public interface UserService {

    List<User> selectAllUser();
    User selectUserByIdentityId(String id);
    User selectUserByUsername(String username);
    User selectUserByOpenid(String openid);
    void insertUser(User user);
    void insertWebUser(User user);
    void insertWXUser(User user);
    void updateUserInfoByID(User user);
    void updateInfoByMyself(User user);
    User selectUserById(String id);
    PageBean<User> selectUserByPage(int size,int page,String sort,String asc);
    int selectCountByUser();
}
