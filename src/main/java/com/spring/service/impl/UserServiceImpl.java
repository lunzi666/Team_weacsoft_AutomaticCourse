package com.spring.service.impl;

import com.spring.dao.UserDao;
import com.spring.entity.PageBean;
import com.spring.entity.User;
import com.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    @Override
    public List<User> selectAllUser() {
        return userDao.selectAllUser();
    }

    @Override
    public User selectUserByIdentityId(String id) {
        return userDao.selectUserByIdentityId(id);
    }

    @Override
    public User selectUserByUsername(String username) {
        return userDao.selectUserByUsername(username);
    }

    @Override
    public User selectUserByOpenid(String openid) {
        return userDao.selectUserByOpenid(openid);
    }

    @Override
    public void insertUser(User user) {
        userDao.insertUser(user);
    }

    @Override
    public void insertWebUser(User user) {
        userDao.insertWebUser(user);
    }

    @Override
    public void insertWXUser(User user) {
        userDao.insertWXUser(user);
    }

    @Override
    public void updateUserInfoByID(User user) {
        userDao.updateUserInfoByID(user);
    }

    @Override
    public void updateInfoByMyself(User user) {
        userDao.updateInfoByMyself(user);
    }

    @Override
    public User selectUserById(String id) {
        return userDao.selectUserById(id);
    }

    @Override
    public PageBean<User> selectUserByPage(int size,int page,String sort,String asc) {
        HashMap<String,Object> map = new HashMap<String,Object>();
        PageBean<User> pageBean = new PageBean<User>();
        //封装当前页数
        pageBean.setCurrPage(page);
        //封装页数大小
        pageBean.setPageSize(size);
        //封装排序字段
        pageBean.setPageSort(sort);
        //封装排序规则
        pageBean.setPageAsc(asc);

        //封装总记录数
        int totalCount = userDao.selectCountByUser();
        pageBean.setTotalCount(totalCount);

        //封装总页数
        double tc = totalCount;
        Double num =Math.ceil(tc/size);//向上取整
        pageBean.setTotalPage(num.intValue());


        map.put("PageStart",(page-1)*size);
        map.put("PageSize", pageBean.getPageSize());
        map.put("PageSort", sort);
        map.put("PageAsc", asc);

        //封装每页显示的数据
        List<User> lists = userDao.selectUserByPage(map);
        pageBean.setLists(lists);
        return pageBean;
    }

    @Override
    public int selectCountByUser() {
        return userDao.selectCountByUser();
    }
}